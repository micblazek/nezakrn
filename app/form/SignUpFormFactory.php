<?php

namespace App\Form;

use Nette\Application\UI\Form;

class SignUpFormFactory
{
    /**
     * @return Form
     */
    public function create()
    {
        $form = new Form;

        $form->addText('username', 'Uživatelské jméno:');
        $form->addPassword('password', 'Heslo:')->setRequired('Zvolte si heslo')->addRule(Form::MIN_LENGTH, 'Heslo musí mít alespoň %d znaky', 3);

        $form->addPassword('passwordVerify', 'Heslo pro kontrolu:')->setRequired('Zadejte prosím heslo ještě jednou pro kontrolu')->addRule(Form::EQUAL, 'Zadané hesla se neshodují', $form['password']);

        $form->addEmail('email', 'E-mail:');
        $form->addText('c', 'Jméno:');
        $form->addText('lastname', 'Příjmení:');
        $form->addSubmit('login', 'Přihlásit se');

        return $form;
    }
}