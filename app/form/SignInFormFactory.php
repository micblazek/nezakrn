<?php

namespace App\Form;

use Nette\Application\UI\Form;


class SignInFormFactory
{
	/**
	 * @return Form
	 */
	public function create()
	{
        $form = new Form;
        $form->addText('username', 'Uživatelské jméno:')
            ->setRequired('Prosím vyplňte své uživatelské jméno.');

        $form->addPassword('password', 'Heslo:')
            ->setRequired('Prosím vyplňte své heslo.');

        $form->addSubmit('send', 'Přihlásit');

		return $form;
	}

}
