<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI;


class BasePresenter extends Nette\Application\UI\Presenter
{
    public function beforeRender()
    {
        parent::beforeRender(); // nezapomeňte volat metodu předka, stejně jako u startup()
        $this->template->menuItems = [
            'Domů' => 'Homepage:',
            'Přihlásit' => 'SignIn:',
            'Odhlásit' => 'SignOut:',
            'Registrovat' => 'Registration:',
            'Přidat otázku' => 'NewQuestion:',
            'Kontakty' => 'Contacts:',
        ];


    }
}
