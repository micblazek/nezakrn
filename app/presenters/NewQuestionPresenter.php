<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI;


class NewQuestionPresenter extends BasePresenter
{
	/** @var Nette\Database\Context */
    private $database;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    protected function createComponentRegistrationForm()
    {
        $category = array(
        'Matematika' => array(
            'Math' => 'Matematika',
            'Geometri' => 'Geometrie',
        ),
        'Jazyky' => array(
            'Czech' => 'Čeština',
            'English' => 'Angličtina',
        ),
        'History' => 'Historie',
        'Law' => 'Právo',
        'Geography' => 'Geografie',
        'Physics' => 'Fyzika',
        'Logic' => 'Logika',
        'Other' => 'Jiné',);   
           
           
        $form = new UI\Form;
        if($this->getUser()->isAllowed('addQuestion')){
            $form->addText('author', 'Autor:')->setRequired('Zadejte prosím jméno');
            $form->addSelect('category', 'Kategorie:', $category)->setPrompt('Zvolte ketogorii')->setRequired('Zadejte prosím kategorii');
            $form->addTextArea('question', 'Otázka:')->setRequired('Zadejte prosím otázku');
            $form->addText('rightAnswer', 'Správná odpověď:')->setRequired('Zadejte prosím správnou odpověď');
            $form->addText('wrongAnswerA', 'Špatná odpověď A:')->setRequired('Zadejte prosím špatnou odpověď A');
            $form->addText('wrongAnswerB', 'Špatná odpověď B')->setRequired('Zadejte prosím špatnou odpověď B');
            $form->addText('wrongAnswerC', 'Špatná odpověď C')->setRequired('Zadejte prosím špatnou odpověď C');
            $form->addSubmit('Save', 'Uložit');
            $form->onSuccess[] = array($this, 'registrationFormSucceeded');
        }else{
             $this->flashMessage('Pro zadaní nové otázky musíš být přihlášen');
        }
        return $form;
    }
    
    public function registrationFormSucceeded(UI\Form $form, $values)
    {
        try {
			$this->database->query('INSERT INTO Question ?', [ // tady můžeme otazník vynechat
				'author'  =>  $values->author,
				'category' => $values->category,
				'question'  => $values->question,
				'rightAnswer'  => $values->rightAnswer,
				'wrongAnswerA'  => $values->wrongAnswerA,
				'wrongAnswerB'  => $values->wrongAnswerB,
				'wrongAnswerC'  => $values->wrongAnswerC,
			]);
		} catch (Nette\Database\UniqueConstraintViolationException $e) {
			throw new DuplicateNameException;
		}
            
        $this->flashMessage('Otázka úspěšně uložena');
        $this->redirect('NewQuestion:');
    }
}
