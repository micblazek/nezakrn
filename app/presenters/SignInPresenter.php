<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;
use App\Form\SignInFormFactory;

class SignInPresenter extends BasePresenter
{
    protected function createComponentSignInForm()
    {
        $form = (new SignInFormFactory)->create();

        $form->onSuccess[] = [$this, 'signInFormSucceeded'];
        return $form;
    }

    public function signInFormSucceeded(Form $form, Nette\Utils\ArrayHash $values)
    {
        try {
            $this->getUser()->login($values->username, $values->password);
            $this->redirect('Homepage:');

            $this->flashMessage($this->getUser()->isAllowed('addQuestion'));

        } catch (Nette\Security\AuthenticationException $e) {
            $form->addError('Nesprávné přihlašovací jméno nebo heslo.');
        }
    }
}