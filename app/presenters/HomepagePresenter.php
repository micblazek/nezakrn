<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI;


class HomepagePresenter extends BasePresenter
{
	/** @var Nette\Database\Context */
    private $database;

    private $answers;
    private $rightIndex;
    private $foo = "any";

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    public function handleRefresh($color){
        $this->foo = rand(1, 100);

        if($this->isAjax()){
            $this->redrawControl('pokus');
        }
    }

    public function RenderDefault()
    { 
        $this->template->foo = $this->foo;
    }

    public function beforeRender()
    {       
        parent::beforeRender(); // nezapomeňte volat metodu předka, stejně jako u startup()
        //$this->template->flashMsgColor = 0;
    	$question = $this->database->table('Question'); 
    	$this->template->questionCount = $question->count('Question');

		$dbActQuestion = $this->database->table('Question')->limit(1)->order('RAND()');
    	
    	foreach ($dbActQuestion as $tmp) {
			if($this->getUser()->isAllowed('answerQuestion')){
		    	$this->template->actQuestion = $tmp->question;

		    	 $startIndex = mt_rand(0, 3);

			    $this->answers[$startIndex] = $tmp->rightAnswer;
				$this->rightIndex = $startIndex;
				$startIndex++;
			    $this->answers[HomepagePresenter::indexControl($startIndex,3)] = $tmp->wrongAnswerA;
			    $startIndex++;
			    $this->answers[HomepagePresenter::indexControl($startIndex,3)] = $tmp->wrongAnswerB;
			    $startIndex++;
			    $this->answers[HomepagePresenter::indexControl($startIndex,3)] = $tmp->wrongAnswerC;
		    }else{
		    	$this->template->actQuestion = "Pro zobrazení otázky se musíš přihlásit :)";
		    }
		}
    }

    protected function createComponentRegistrationForm()
    {
    	$form = new UI\Form;
    	if($this->getUser()->isAllowed('answerQuestion')){
	    	$values = array('1', '2', '3', '4');

	        $possibleAnswer = array(
	        '0'=> $this->answers[0],
	        '1'=> $this->answers[1],
	        '2' => $this->answers[2],
	        '3' => $this->answers[3]);  

	        $form->addRadioList('Answer', 'Odpověď:', $possibleAnswer);
	        $form->addSubmit('Send', 'Odeslat');
	        $form->addHidden('RightAnswer', $this->rightIndex);
	        $form->onSuccess[] = array($this, 'registrationFormSucceeded');
	
        }
        return $form;
    }
    
    public function registrationFormSucceeded(UI\Form $form, $values)
    {
     	  $msg = "Špatně";
     	  $this->template->flashMsgColor = 2;
          if($values->Answer==$values->RightAnswer){
          	$msg = "Správně";
          	$this->template->flashMsgColor = 1;
          }

        $this->flashMessage($msg);
        $this->redirect('Homepage:');
    }

    private function indexControl($index, $max){
    	if($index<=$max){
    		return $index;
    	}else{
    		return $index - $max - 1;
    	}
    }
}
