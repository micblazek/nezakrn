<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;


class SignOutPresenter extends BasePresenter
{
    protected function createComponentSignInForm()
    {
        $form = new Form;
        $form->addSubmit('signOut', 'Odhlásit');

        $form->onSuccess[] = [$this, 'signOutFormSucceeded'];
        return $form;
    }

    public function signOutFormSucceeded(Form $form, Nette\Utils\ArrayHash $values)
    {
        $this->getUser()->logout();
        $this->redirect('Homepage:');
    }
}