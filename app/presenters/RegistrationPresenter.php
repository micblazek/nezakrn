<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI;
use Nette\Security\Passwords;
use App\Form\SignUpFormFactory;


class RegistrationPresenter extends BasePresenter
{
    /** @var Nette\Database\Context */
    private $database;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    protected function createComponentSignInForm()
    {
        $form = (new SignUpFormFactory)->create();
        $form->onSuccess[] = [$this, 'signInFormSubmitted']; // a přidat událost po odeslání

        return $form;
    } 

    public function signInFormSubmitted(UI\Form $form, $values)
    {
        try {
            try {
                $this->database->query('INSERT INTO users ?', [ // tady můžeme otazník vynechat
                    'username'  =>  $values->username,
                    'password' => Passwords::hash($values->password),
                    'email'  => $values->email,
                    'surname'  => $values->lasttname,
                    'firstname'  => $values->firstname,
                ]);
            } catch (Nette\Database\UniqueConstraintViolationException $e) {
                //throw new DuplicateNameException;
                $this->flashMessage('Uživatelské jméno obsazeno');
            }

            $this->redirect('Homepage:');
            $this->flashMessage('Uživatel registrován');
        } catch (Nette\Security\AuthenticationException $e) {
            $form->addError('Registrace se nezdařila');
        }
    }  
}
